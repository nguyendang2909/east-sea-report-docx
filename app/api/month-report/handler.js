/* eslint-disable no-restricted-syntax */
import moment from 'moment';
import fs from 'fs';
import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import db from '../../lib/db';
import { formatStringToText } from '../../lib/function';
import { MONTH_REPORT_PATH } from '../../../config.json';

export default async function exportReportToDocx(req, res) {
  const { month } = req.query;
  const year = req.query.year || moment().format('YYYY');

  if (!req.query || !month || !year) {
    res.sendStatus(400);
    return;
  }

  // months start at index 0 in momentjs, so we subtract 1
  const startDate = moment([year, month - 1, 1]).format('YYYY-MM-DD');
  const startDateInVnFormat = moment([year, month - 1, 1]).format('DD-MM-YYYY');

  // get the number of days for this month
  const daysInMonth = moment(startDate).daysInMonth();

  // we are adding the days in this month to the start date (minus the first day)
  const endDate = moment(startDate).add(daysInMonth - 1, 'days').format('YYYY-MM-DD');
  const endDateInVnForrmat = moment(startDate).add(daysInMonth - 1, 'days').format('DD/MM/YYYY');

  const [opinionsContent] = await db.query(
    `SELECT a.note, b.name as country FROM olala3w_opinion a
    LEFT JOIN olala3w_country b ON a.countryId = b.countryId
    WHERE a.isActive = 1 AND b.isActive = 1 AND a.time BETWEEN '${startDate}' AND '${endDate}'
    ORDER BY a.time ASC`,
  );

  const docxOpinions = [];

  for (const opinionContent of opinionsContent) {
    const hasCountry = docxOpinions.find((e) => e.opinionCountry === opinionContent.country);
    if (!hasCountry) {
      docxOpinions.push({
        opinionCountry: opinionContent.country,
        opinionComment: [{ opComment: opinionContent.note }],
      });
    } else {
      hasCountry.opinionComment.push({ opComment: opinionContent.note });
    }
  }

  const docxFS = [];

  const [fieldSituationsContent] = await db.query(
    `SELECT d.name as country, e.name as ship, c.title as zone, a.purpose, a.note, a.file, a.time FROM olala3w_field_situation
    a LEFT JOIN olala3w_local c ON a.localId = c.local_id
    LEFT JOIN olala3w_country d ON a.countryId = d.countryId
    LEFT JOIN olala3w_ship e ON a.shipId = e.shipId
    WHERE a.isActive = 1 AND c.is_active = 1 AND d.isActive = 1 AND a.time BETWEEN '${startDate}' AND '${endDate}'
    ORDER BY a.time ASC`,
  );

  // console.log(fieldSituationsContent);
  for (const fieldSituationContent of fieldSituationsContent) {
    const hasCountry = docxFS.find((e) => e.fsCountry === fieldSituationContent.country);
    const shipInfo = {
      fsTime: fieldSituationContent.time.trim(),
      shipName: fieldSituationContent.ship.trim(),
      zone: fieldSituationContent.zone.trim(),
      purpose: formatStringToText(fieldSituationContent.purpose),
      shipComment: formatStringToText(fieldSituationContent.note),
    };
    if (!hasCountry) {
      docxFS.push({
        fsCountry: fieldSituationContent.country,
        ship: [shipInfo],
      });
    } else {
      hasCountry.ship.push(shipInfo);
    }
  }

  const content = fs.readFileSync('./app/data/template/template.docx', 'binary');
  const zip = new PizZip(content);
  const doc = new Docxtemplater();
  doc.loadZip(zip);

  doc.setData({
    startDate: startDateInVnFormat,
    endDate: endDateInVnForrmat,
    docxOpinions,
    docxFS,
  });

  await doc.render();
  const buf = doc.getZip().generate({ type: 'nodebuffer' });

  const REPORT_PATH = `${MONTH_REPORT_PATH}/${year}-${month}.docx`;
  fs.writeFileSync(REPORT_PATH, buf);

  if (fs.existsSync(MONTH_REPORT_PATH) && fs.existsSync(REPORT_PATH)) {
    const downloadFileName = `${year}-${month}.docx`;
    res.download(REPORT_PATH, downloadFileName);
  } else res.sendStatus(404);
}
