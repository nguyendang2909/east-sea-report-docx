import express from 'express';
import asyncHandler from 'express-async-handler';
import exportMonthReportToDocx from './handler';

const router = express.Router();

router.get('/', asyncHandler(exportMonthReportToDocx));

export default router;
