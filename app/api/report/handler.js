/* eslint-disable no-restricted-syntax */
import { Op } from 'sequelize';
import moment from 'moment';
import fs from 'fs';
import PizZip from 'pizzip';
import Docxtemplater from 'docxtemplater';
import OpinionEntity from '../../model/opinion';
import CountryEntity from '../../model/country';
import db from '../../lib/db';
import { formatStringToText } from '../../lib/function';

export default async function exportReportToDocx(req, res) {
  const queryStartDate = req.query.startDate;
  let queryEndDate = req.query.endDate;
  if (!queryEndDate || queryEndDate === '__/__/____') queryEndDate = moment().format('DD/MM/YYYY');

  if (!req.query || !queryStartDate || queryStartDate === '__/__/____') {
    res.sendStatus(400);
    return;
  };

  const startDate = moment(req.query.startDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
  const endDate = moment(queryEndDate, 'DD/MM/YYYY').format('YYYY-MM-DD');

  const [opinionsContent] = await db.query(
    `SELECT a.note, b.name as country FROM olala3w_opinion a
    LEFT JOIN olala3w_country b ON a.countryId = b.countryId
    WHERE a.isActive = 1 AND b.isActive = 1 AND a.time BETWEEN '${startDate}' AND '${endDate}'
    ORDER BY a.time ASC`,
  );

  const docxOpinions = [];

  for (const opinionContent of opinionsContent) {
    const hasCountry = docxOpinions.find((e) => e.opinionCountry === opinionContent.country);
    if (!hasCountry) {
      docxOpinions.push({
        opinionCountry: opinionContent.country,
        opinionComment: [{ opComment: opinionContent.note }],
      });
    } else {
      hasCountry.opinionComment.push({ opComment: opinionContent.note });
    }
  }

  const docxFS = [];

  const [fieldSituationsContent] = await db.query(
    `SELECT d.name as country, e.name as ship, c.title as zone, a.purpose, a.note, a.file, a.time FROM olala3w_field_situation
    a LEFT JOIN olala3w_local c ON a.localId = c.local_id
    LEFT JOIN olala3w_country d ON a.countryId = d.countryId
    LEFT JOIN olala3w_ship e ON a.shipId = e.shipId
    WHERE a.isActive = 1 AND c.is_active = 1 AND d.isActive = 1 AND a.time BETWEEN '${startDate}' AND '${endDate}'
    ORDER BY a.time ASC`,
  );

  // console.log(fieldSituationsContent);
  for (const fieldSituationContent of fieldSituationsContent) {
    const hasCountry = docxFS.find((e) => e.fsCountry === fieldSituationContent.country);
    const shipInfo = {
      fsTime: fieldSituationContent.time.trim(),
      shipName: fieldSituationContent.ship.trim(),
      zone: fieldSituationContent.zone.trim(),
      purpose: formatStringToText(fieldSituationContent.purpose),
      shipComment: formatStringToText(fieldSituationContent.note),
    };
    if (!hasCountry) {
      docxFS.push({
        fsCountry: fieldSituationContent.country,
        ship: [shipInfo],
      });
    } else {
      hasCountry.ship.push(shipInfo);
    }
  }

  const content = fs.readFileSync('./app/data/template/template.docx', 'binary');
  const zip = new PizZip(content);
  const doc = new Docxtemplater();
  doc.loadZip(zip);

  doc.setData({
    startDate: queryStartDate,
    endDate: queryEndDate,
    docxOpinions,
    docxFS,
  });

  await doc.render();
  const buf = doc.getZip().generate({ type: 'nodebuffer' });
  fs.writeFileSync('./data/output.docx', buf);

  if (fs.existsSync('./data/') && fs.existsSync('./data/output.docx')) {
    const downloadFileName = 'bao_cao_bien_dong.docx';
    res.download('./data/output.docx', downloadFileName);
  } else res.sendStatus(404);
}
