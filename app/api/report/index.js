import express from 'express';
import asyncHandler from 'express-async-handler';
import exportReportToDocx from './handler';

const router = express.Router();

router.get('/', asyncHandler(exportReportToDocx));

export default router;
