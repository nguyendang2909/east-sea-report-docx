import { Sequelize } from 'sequelize';
import loggerFactory from '../loggerFactory';
import { DB_HOST, DB_NAME, DB_PORT, DB_TYPE, DB_USERNAME, DB_PASSWORD } from '../../../config.json';

const logger = loggerFactory.getLogger(__filename);

const db = new Sequelize(
  DB_NAME,
  DB_USERNAME,
  DB_PASSWORD,
  {
    dialect: DB_TYPE,
    host: DB_HOST,
    port: DB_PORT,
    logging: (msg) => logger.debug({ message: msg }),
  },
);

export async function dbAuthenticate() {
  await db.authenticate();
  logger.info({ message: 'Kết nối thành công vào CSDL' });
}

export default db;
