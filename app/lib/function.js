export function lcFirstLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

export function removeLastDot(string) {
  return string.replace(/.\s*$/, '');
}

export function formatStringToText(string) {
  let result = string.trim();
  // To lower case first character
  result = result.charAt(0).toLowerCase() + result.slice(1);
  // Remove last dot or comma
  const lastLetter = result.charAt(result.length - 1);
  if (lastLetter === ',' || lastLetter === '.') result = result.slice(0, -1);

  return result;
}
