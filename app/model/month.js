import { Model, DataTypes } from 'sequelize';
import db from '../lib/db';

export default class MonthReportEntity extends Model {}

MonthReportEntity.init(
  {
    monthReportId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    month: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
    },
    year: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize: db,
    modelName: 'olala3w_country',
    freezeTableName: true,
  },
);

db.sync({ force: false });
