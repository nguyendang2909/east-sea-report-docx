import loggerFactory from './lib/loggerFactory';
import services from './services';
import { dbAuthenticate } from './lib/db'

const logger = loggerFactory.getLogger(__filename);

(async () => {
  try {
    await services.start();
    await dbAuthenticate();
    logger.info({ message: 'Application initialize' });
  } catch (err) {
    logger.error({ message: err.stack || err });
  }
})();

['SIGTERM', 'SIGINT'].forEach((signal) => {
  process.on(signal, async () => {
    try {
      services.shutdown();
      logger.info({ message: 'Server shutdown' });
      process.exit(0);
    } catch (err) {
      logger.error({ message: 'Unable to shutdown application' });
      process.exit(1);
    }
  });
});
