import server from './server';

class Services {
  constructor() {
    this.server = '';
  }

  start() {
    this.server = server;
    this.server.start();
  }

  shutdown() {
    this.server.shutdown();
  }
}

export default new Services();
