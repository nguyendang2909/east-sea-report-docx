import express from 'express';
import bodyParser from 'body-parser';
import loggerFactory from '../../lib/loggerFactory';
import config from '../../../config.json';
import reportRouter from '../../api/report';
import monthReportRouter from '../../api/month-report';

const logger = loggerFactory.getLogger(__filename);

const app = express();

class Server {
  constructor() {
    this.server = '';
  }

  async start() {
    const { PORT } = config;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use('/report', reportRouter);
    app.use('/month-report', monthReportRouter);
    this.server = app.listen(PORT, () => {
      logger.info({ message: `Server listening on port ${PORT}` });
    });
  }

  shutdown() {
    this.server.close();
  }
}

export default new Server();
